import React, {Component} from 'react';
import {View, Image, Alert, StyleSheet, Button, TextInput} from 'react-native';

export default class Login extends Component {
	constructor(props) {
    	super(props);
    	this.state = {text: ''};
  	}
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.inner}>
					<Image style={styles.image} source={require('../images/logo.png')}/>
					<TextInput
          				style={{height: 40, borderWidth: 1, width: 250 }}
          				placeholder="E-mail"
          				onChangeText={(text) => this.setState({text})}
          			/>
          			<TextInput
          				style={{height: 40, borderWidth: 1, width: 250}}
          				placeholder="Password"
          				onChangeText={(text) => this.setState({text})}
          			/>
          		<Button
  					onPress={() => {
    					Alert.alert('You login!');
 					}}
  					title="Login"
				/>
				</View>
			</View>

	);}
}

const styles = StyleSheet.create({
	container: {
	    borderRadius: 4,
	    borderWidth: 0.5,
	    backgroundColor: '#FFDA45',
	    height: '100%'
	},
	inner: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center' 
	},

	image: {
	    width: 200,
	    height: 200
  	}
});